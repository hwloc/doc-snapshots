#!/usr/bin/bash

echo
echo "###########################"
echo "# Download previous pages #"
echo "###########################"
echo
curl --output artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.inria.fr/api/v4/projects/46438/jobs/artifacts/main/download?job=pages"
ls -l artifacts.zip
unzip artifacts.zip
mv public www

echo
echo "##########################################"
echo "# Removing previous doc snapshots: $REMOVE"
echo "##########################################"
echo
if test "x$REMOVE" = xall; then
  echo "### Removing all"
  rm -rf www
else if test "x$REMOVE" != xnone; then
  for i in $REMOVE; do
    echo "### Removing $i ?"
    if test -f "www/${i}-branchname.txt"; then
      echo "### Removing $i"
      rm -rf "www/${i}-*"
    fi
  done
fi fi

echo
echo "###############################################"
echo "# Generating requested branches: $BRANCHES"
echo "###############################################"
echo
for i in $BRANCHES; do
  echo
  echo "#############################"
  echo "### Generating branch $i"
  echo "#############################"
  ./generate-doc.sh $i
done

echo
echo "####################"
echo "# Generating index #"
echo "####################"
echo
mkdir -p www
rm -f www/index.html

cat >> www/index.html << EOF
<html>
<body>
<table border="1">
<tr>
 <td>Branch</td>
 <td>Git Commit</td>
 <td>Date</td>
 <td colspan="4">Docs</td>
 <td>Published</td>
</tr>
EOF

oneentry() {
  branchfile="$1"
  echo "##### Listing $branchname"
  branchname=$(cat $branchfile)
  gitcommit=$(cat www/$branchname-gitcommit.txt | tr -d '\n')
  gitdate=$(cat www/$branchname-gitdate.txt | tr -d '\n')
  filedate=$(date +%Y%m%d.%H%M --date @$(stat -c %Y $branchfile))
  cat >> www/index.html << EOF
 <tr>
  <td>$branchname</td>
  <td>$gitcommit</td>
  <td>$gitdate</td>
  <td><a href="$branchname-html/index.html">HTML</a></td>
  <td><a href="$branchname-a4.pdf">A4 PDF</a></td>
  <td><a href="$branchname-letter.pdf">Letter PDF</a></td>
  <td><a href="$branchname-README">README</a></td>
  <td>$filedate</td>
 </tr>
EOF
}

splitentries() {
  cat >> www/index.html << EOF
 <tr>
 </tr>
EOF
}

splitentries

officialbranches=$((ls www/master-branchname.txt 2>/dev/null && ls -r www/v[0-9].[0-9x]*-branchname.txt 2>/dev/null) | xargs)
echo "### Found official branches: $officialbranches"
if test -n "$officialbranches"; then
  for i in $officialbranches; do
    oneentry $i
    touch ${i}.done
  done
fi

splitentries

PRbranches=$(ls www/PR-*-branchname.txt 2>/dev/null | xargs)
echo "### Found PR: $PRbranches"
if test -n "$PRbranches"; then
  for i in $PRbranches; do
    oneentry $i
    touch ${i}.done
  done
fi

splitentries

otherbranches=$(ls www/*-branchname.txt 2>/dev/null | xargs)
echo "### Listing remaining branches"
if test -n "$otherbranches"; then
  for i in $otherbranches; do
    if ! test -f ${i}.done; then
      oneentry $i
      touch ${i}.done
    fi
  done
fi

splitentries

rm -f www/*-branchname.txt.done

indexdate=`date +%Y%m%d.%H%M`

cat >> www/index.html << EOF
</table>
<p>Updated on $indexdate</p>
</body>
</html>
EOF
