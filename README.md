# Publish hwloc documentation for current git Branches

## Variables for pipeline

to publish the doc of some specific branches:
BRANCHES="master v2.x v2.9"
possible values include "PR-654" or "bgoglin".

to remove some existing docs:
REMOVE=none
REMOVE=all
REMOVE="PR-654 bgoglin"
