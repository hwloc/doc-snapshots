#!/bin/bash

set -e

branch=$1

if test "x$branch" = x; then
  echo "Branch parameter missing"
  exit 1
fi

test -e "tmp-$branch" && rm -rf "tmp-$branch"
mkdir "tmp-$branch"
cd "tmp-$branch"

if test "${branch#PR-}" != "$branch"; then
  wget 'https://ci.inria.fr/hwloc/job/basic/view/change-requests/job/'$branch'/lastSuccessfulBuild/artifact/*.gz/*zip*/archive.zip'
else if test "$branch" = "bgoglin"; then
  wget 'https://ci.inria.fr/hwloc/view/all/job/bgoglin/lastSuccessfulBuild/artifact/*.gz/*zip*/archive.zip'
else
  wget 'https://ci.inria.fr/hwloc/job/basic/job/'$branch'/lastSuccessfulBuild/artifact/*.gz/*zip*/archive.zip'
fi fi

if ! test -f archive.zip; then
  echo "Failed to download archive.zip"
  exit 1
fi

unzip archive.zip
tarball=$(ls *.tar.gz)
basename=$(basename $tarball .tar.gz)
tar xfz $tarball
cd $basename
#./configure
#cd doc
#doxygen www.open-mpi.org.cfg
#cd -

mkdir -p ../../www/
#cp -a doc/www.open-mpi.org/html ../../www/html-$branch
echo $branch > ../../www/${branch}-branchname.txt
echo $tarball | sed -r -e 's/.*git(.*).tar.gz/\1/' > ../../www/${branch}-gitcommit.txt
echo $tarball | sed -r -e 's/.*-(.*)\.git.*/\1/' > ../../www/${branch}-gitdate.txt
rm -rf ../../www/${branch}-html
cp -a doc/doxygen-doc/html ../../www/${branch}-html
cp doc/doxygen-doc/hwloc-a4.pdf ../../www/${branch}-a4.pdf
cp doc/doxygen-doc/hwloc-letter.pdf ../../www/${branch}-letter.pdf
cp README ../../www/${branch}-README
